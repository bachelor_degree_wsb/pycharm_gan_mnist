1. https://livebook.manning.com/book/deep-learning-with-pytorch/chapter-9 *book about processing ct scans*
2. Eric J. Olson, “Lung nodules: Can they be cancerous?” Mayo Clinic, http://mng.bz/yyge.
3. https://www.frontiersin.org/articles/10.3389/fdgth.2020.609349/full *a lot of nodules datasets*
4. https://theaisummer.com/medical-image-coordinates/#anatomical-coordinate-system
5. https://wiki.cancerimagingarchive.net/display/Public/LIDC-IDRI#1966254f633413761b746ff9e49dd8f0d5b679d **nodule database**
6. http://medicaldecathlon.com/#tasks **nodule database**
7. https://nihcc.app.box.com/v/DeepLesion **large nodule db**
8. https://github.com/sangwoomo/FreezeD **improve for GAN training **
9. https://arxiv.org/abs/1809.11096 **even better GAN?**