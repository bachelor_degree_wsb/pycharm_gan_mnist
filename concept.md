deep-learning-with-pytorch
---
A mass of tissue made of proliferating cells in the lung is a tumor. A tumor can be benign
or it can be malignant, in which case it is also referred to as cancer. A small tumor in
the lung (just a few millimeters wide) is called a nodule. About 40% of lung nodules turn
out to be malignant—small cancers. It is very important to catch those as early as possible, and this depends on medical imaging of the kind we are looking at here.
---
We want to have 2 generators (?), one with nodules over 3mm 
---
Lung nodules are usually about 0.2 inch (5 millimeters) to 1.2 inches (30 millimeters) in size. A larger lung nodule, such as one that's 30 millimeters or larger, is more likely to be cancerous than is a smaller lung nodule.
---
For now, it’s enough to know that it’s much, much
faster to read in 215 float32 values from disk than it is to read in 225 int16 values, convert to float32, and then select a 215 subset.
---
Please subtract 32768 from the16-bit pixel intensitiesto obtain the original Hounsfield unit (HU) values.
----
The situation changes if this rotation is only executed at a probability p < 1: this increases the
relative occurrence of 0
◦
, and now the augmented distributions can match only if the generated
images have correct orientation. Similarly, many other stochastic augmentations can be designed to
be non-leaking on the condition that they are skipped with a non-zero probability

---

 The “Kimg” refers to the iterations of training process.
 
----

We always use the same value of p for all transformations. The randomization is done separately for each
augmentation and for each image in a minibatch.

-------
We control the augmentation strength p as follows. We initialize p to zero and adjust its value once
every four minibatches2 based on the chosen overfitting heuristic. If the heuristic indicates too
much/little overfitting, we counter by incrementing/decrementing p by a fixed amount. We set the
adjustment size so that p can rise from 0 to 1 sufficiently quickly, e.g., in 500k images. After every
step we clamp p from below to 0. We call this variant adaptive discriminator augmentation (ADA).
----

https://www.quora.com/What-is-an-exponential-moving-average-I-am-very-confused-about-its-purpose-in-machine-learning-If-you-can-explain-it-in-simple-terms-that-would-be-much-appreciated

---
https://machinelearningmastery.com/generative-adversarial-network-loss-functions/
https://neptune.ai/blog/gan-loss-functions
-------