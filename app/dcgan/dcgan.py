from __future__ import annotations
import time

from time import time
import torch.nn as nn
import torch
import torchvision as tv
import matplotlib.pyplot as plt
from torch import optim
import torch.utils.data

from ada.augmentation import AdaptiveAugmentation
from app.dcgan.discriminator import Discriminator
from app.dcgan.generator import Generator
from jpg_dataset_loader import LidcCtImagesDataset

class DCGAN:
    def __init__(self, latent_dim, noise_fn,
                 batch_size=32, device='cuda:0', lr_d=1e-3, lr_g=2e-4, augment_p=0, apply_aug = True):

        self.apply_aug = apply_aug
        self.generator = Generator(latent_dim).to(device)
        self.discriminator = Discriminator().to(device)
        self.noise_fn = noise_fn
        self.dataloader = None
        self.batch_size = batch_size
        self.device = device
        self.criterion = nn.BCELoss()
        self.optim_d = optim.Adam(self.discriminator.parameters(),
                                  lr=lr_d, betas=(0.5, 0.999))
        self.optim_g = optim.Adam(self.generator.parameters(),
                                  lr=lr_g, betas=(0.5, 0.999))

        self._d_real_scores = []
        self.aug = AdaptiveAugmentation(augment_p, 0.6, 500, batch_size)

    def init_data(self):
        transform = tv.transforms.Compose([
            tv.transforms.ToPILImage(),
            tv.transforms.Grayscale(num_output_channels=1),
            tv.transforms.ToTensor(),
            tv.transforms.Resize([64,64]),
            tv.transforms.Normalize((0.5,), (0.5,))
        ])

        self.dataloader = LidcCtImagesDataset.build(transform, r'../64x64_png/*.png', self.batch_size,1)
        return self

    def save_models(self):
        torch.save(self.generator.state_dict(), "../models/generator_100-1000.pth")
        print("Saved PyTorch Model State to generator.pth")

        torch.save(self.discriminator.state_dict(), "../models/discriminator_100-1000.pth")
        print("Saved PyTorch Model State to discriminator.pth")

    def generate_samples(self, latent_vec=None, num=None):
        num = self.batch_size if num is None else num
        latent_vec = self.noise_fn(num) if latent_vec is None else latent_vec
        with torch.no_grad():
            samples = self.generator(latent_vec)
        samples = samples.cpu()  # move images to cpu
        return samples

    def train_step_generator(self, samples):
        """Train the generator one step and return the loss."""
        self.generator.zero_grad()

        latent_vec = self.noise_fn(samples.size(0))
        generated = self.generator(latent_vec)
        if self.apply_aug:
            generated = self.aug.augment(generated).to(self.device)
        classifications = self.discriminator(generated)

        target_ones = torch.ones((len(samples), 1), device=self.device)
        loss = self.criterion(classifications, target_ones)
        loss.backward()
        self.optim_g.step()
        return loss.item()

    def train_step_discriminator(self, real_samples):
        """Train the discriminator one step and return the losses."""
        self.discriminator.zero_grad()

        # real samples
        pred_real = self.discriminator(real_samples)
        self._d_real_scores.append((pred_real, len(real_samples)))
        target_ones = torch.ones((len(real_samples), 1), device=self.device)
        loss_real = self.criterion(pred_real, target_ones)

        # generated samples
        latent_vec = self.noise_fn(real_samples.size(0))
        with torch.no_grad():
            fake_samples = self.generator(latent_vec)
        pred_fake = self.discriminator(fake_samples)
        target_zeros = torch.zeros((len(real_samples), 1), device=self.device)
        loss_fake = self.criterion(pred_fake, target_zeros)

        # combine
        loss = (loss_real + loss_fake) / 2
        loss.backward()
        self.optim_d.step()
        return loss_real.item(), loss_fake.item()

    def train_epoch(self, print_frequency=10, max_steps=0):
        """Train both networks for one epoch and return the losses.
        Args:
            print_frequency (int): print stats every `print_frequency` steps.
            max_steps (int): End epoch after `max_steps` steps, or set to 0
                             to do the full epoch.
        """
        loss_g_running, loss_d_real_running, loss_d_fake_running, batch = 0, 0, 0, 1
        for batch, real_samples in enumerate(self.dataloader, start=1):
            if batch % 4 == 0 and self.apply_aug:
                score_sum = 0
                samples = 0
                for score, samples_len in self._d_real_scores:
                    score_sum += score
                    samples += samples_len

                self.aug.tune(score_sum/samples if samples > 0 else 0, self.batch_size * 4)


            real_samples = real_samples.to(self.device)
            if self.apply_aug:
                real_samples = self.aug.augment(real_samples).to(self.device)

            ldr_, ldf_ = self.train_step_discriminator(real_samples)

            loss_d_real_running += ldr_
            loss_d_fake_running += ldf_

            loss_g_running += self.train_step_generator(real_samples)

            if print_frequency and (batch) % print_frequency == 0:
                print(f"{batch}/{len(self.dataloader)}:"
                      f" G={loss_g_running / (batch ):.3f},"
                      f" Dr={loss_d_real_running / (batch):.3f},"
                      f" Df={loss_d_fake_running / (batch):.3f}",
                      end='\r',
                      flush=True)
            if max_steps and batch == max_steps:
                break
        if print_frequency:
            print()

        loss_g_running /= batch
        loss_d_real_running /= batch
        loss_d_fake_running /= batch
        return loss_g_running, (loss_d_real_running, loss_d_fake_running)


def main():
    batch_size = 32
    epochs = 1
    latent_dim = 100
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(f"Device used: {device}")
    noise_fn = lambda x: torch.randn((x, latent_dim), device=device)

    gan = DCGAN(latent_dim, noise_fn, batch_size=batch_size, device=device)
    gan.init_data()

    start = time()
    for i in range(epochs):
        # torch.backends.cudnn.benchmark = True  # Improves training speed.
        # conv2d_gradfix.enabled = True  # Improves training speed.
        # grid_sample_gradfix.enabled = True

        print(f"Epoch {i + 1}; Elapsed time = {int(time() - start)}s")
        gan.train_epoch()

    gan.save_models()
    images = gan.generate_samples()
    ims = tv.utils.make_grid(images, normalize=True)
    plt.imshow(ims.numpy().transpose((1, 2, 0)))
    plt.show()


if __name__ == '__main__':
    main()
