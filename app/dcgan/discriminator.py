from __future__ import annotations
import torch.nn as nn
from torch import Tensor


class Discriminator(nn.Module):
    def __init__(self):
        """A discriminator for discerning real from generated images.
        Images must be single-channel and 28x28 pixels.
        Output activation is Sigmoid.
        """
        super(Discriminator, self).__init__()
        self._init_modules()

    def _init_modules(self):
        """Initialize the modules."""
        self.conv1 = nn.Conv2d(
            in_channels=1,
            out_channels=64,
            kernel_size=5,
            stride=2,
            padding=2,
            bias=True)
        self.leaky_relu = nn.LeakyReLU()
        self.dropout_2d = nn.Dropout2d(0.3)

        self.conv2 = nn.Conv2d(
            in_channels=64,
            out_channels=128,
            kernel_size=5,
            stride=2,
            padding=2,
            bias=True)

        self.linear1 = nn.Linear(128 * 16 * 16, 1, bias=True) # those 12 are image sizes and 128 is out channel
        self.sigmoid = nn.Sigmoid()

    def forward(self, input_tensor: Tensor):
        """Forward pass; map samples to confidence they are real [0, 1]."""
        intermediate = self.conv1(input_tensor) #32x1x64x64
        intermediate = self.leaky_relu(intermediate) #32x64x32x32
        intermediate = self.dropout_2d(intermediate) #32x64x32x32

        intermediate = self.conv2(intermediate) #32x64x32x32
        intermediate = self.leaky_relu(intermediate)
        intermediate = self.dropout_2d(intermediate)

        intermediate = intermediate.view((-1, 128 * 16 * 16))
        intermediate = self.linear1(intermediate)
        output_tensor = self.sigmoid(intermediate)

        return output_tensor