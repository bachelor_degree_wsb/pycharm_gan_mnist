from __future__ import annotations
import torch.nn as nn
from torch import Tensor


class Generator(nn.Module):
    def __init__(self, latent_dim=100):
        super(Generator, self).__init__()
        self.latent_dim = latent_dim
        self._init_modules()

    def _init_modules(self):
        """Initialize the modules."""
        # Project the input
        self.linear1 = nn.Linear(self.latent_dim, 256 * 16 * 16, bias=False)
        self.bn1d1 = nn.BatchNorm1d(256 * 16 * 16)
        self.leaky_relu = nn.LeakyReLU()

        # Convolutions
        self.conv1 = nn.ConvTranspose2d(
            in_channels=256,
            out_channels=128,
            kernel_size=5,
            stride=1,
            padding=2,
            bias=False)
        self.bn2d1 = nn.BatchNorm2d(128)

        self.conv2 = nn.ConvTranspose2d(
            in_channels=128,
            out_channels=64,
            kernel_size=4,
            stride=2,
            padding=1,
            bias=False)
        self.bn2d2 = nn.BatchNorm2d(64)

        self.conv3 = nn.ConvTranspose2d(
            in_channels=64,
            out_channels=1,
            kernel_size=4,
            stride=2,
            padding=1,
            bias=False)
        self.tanh = nn.Tanh()

    def forward(self, input_tensor: Tensor):
        """Forward pass; map latent vectors to samples."""
        intermediate = self.linear1(input_tensor)
        intermediate = self.bn1d1(intermediate)
        intermediate = self.leaky_relu(intermediate)

        intermediate = intermediate.view((-1, 256, 16, 16))

        intermediate = self.conv1(intermediate)
        intermediate = self.bn2d1(intermediate)
        intermediate = self.leaky_relu(intermediate)

        intermediate = self.conv2(intermediate)
        intermediate = self.bn2d2(intermediate)
        intermediate = self.leaky_relu(intermediate)

        intermediate = self.conv3(intermediate)
        output_tensor = self.tanh(intermediate)
        return output_tensor