import numpy as np
from numpy import ndarray


def normalize_ct_image(img: ndarray) -> ndarray:
    return (img - np.min(img)) / (np.max(img) - np.min(img))

def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.299, 0.587, 0.144])