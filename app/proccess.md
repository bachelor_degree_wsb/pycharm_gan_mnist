~~1. Download data (probably external disk will be needed).~~ [DONE]
~~2. Retrieve all malignant (no definition of malignant) nodules .mhd files.~~ [DONE]
~~3. Retrieve nodules with image 64x64 via axis x(or 0 or I) with nodule in the middle.~~ [DONE]
4. Put that images through DCGAN
5. Train model for nodules detection (with window 64x64?) (only for axis 0)
6. Train model for malignant detection  (only for axis 0)
7. Test model on new data
--------

3. Retrieve nodules with image 64x64 via axis x(or 0 or I) with nodule in the middle.
   ~~3.1 Create class for handling every CT image~~~[DONE]
   ~~3.2 Add logic for retrieving nodule explicitly (if has any)~~ [DONE]
   ~~3.3 Add dataloader for Luna~~ [DONE]
   ~~3.4 Add dataloader for LIDC-IDRI~~ [DONE]
   3.5 Integrate rest of datasets (optional)
   
4. Put that image through DCGAN
   ~~4.1 There is need to build DCGAN with PyTorch and usage of CUDA.~~ [DONE]
   4.2 Decide to use either GCP or my own GPUS
   4.3 Take a look through papers:
        - https://proceedings.neurips.cc/paper/2020/hash/90525e70b7842930586545c6f1c9310c-Abstract.html
        - https://proceedings.neurips.cc/paper/2020/hash/8d30aa96e72440759f74bd2306c1fa3d-Abstract.html
   4.4 Test model, validate, save weights and other metrics