import pylidc as pl
import torch
from numpy import ndarray
from torch.utils.data import Dataset
import logging as log

from lidc_idri_dcom.ct_image import CtImage


class LidcCtImagesMalignantOnlyDataset(Dataset):
    def __init__(self, min_level: int = 4,  transform=None):
        annotations = pl.query(pl.Annotation.id) \
            .filter(pl.Annotation.malignancy >= min_level)

        self.samples = annotations.all()
        self.length = annotations.count()
        self.transform = transform
        self.ct_image = CtImage([64, 64])

        log.info("{!r}: {} {} samples".format(
            self,
            self.length,
            "training",
        ))

    def __len__(self):
        return self.length

    def __getitem__(self, ndx: int) -> ndarray:
        sample = self.ct_image.retrieve_raw_candidate(self.samples[ndx][0])
        if self.transform:
            sample = self.transform(sample)
        return (sample, self.samples[ndx][0])

    @classmethod
    def build(cls, min_lvl, transform, batch_size, num_workers):
        ds = cls(min_level=min_lvl, transform=transform)
        return torch.utils.data.DataLoader(ds, batch_size=batch_size,
                                           shuffle=True, num_workers=num_workers)