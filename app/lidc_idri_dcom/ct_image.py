import functools
import logging as log

import numpy as np
import pylidc as pl

from numpy import ndarray


class CtImage:

    def __init__(self,  boundaries: ndarray):
        self.boundaries = boundaries

    @functools.lru_cache()
    def retrieve_raw_candidate(self,annotation_id: int) -> ndarray:
        annotation = pl.query(pl.Annotation) \
            .filter(pl.Annotation.id == annotation_id).first()

        slice_list = []
        ct_image = annotation.scan.to_volume()
        center_irc = annotation.centroid

        for axis, boundary in enumerate(self.boundaries):
            center_val = center_irc[axis]
            # boundaries checking
            assert 0 <= center_val < ct_image.shape[axis]

            start_ndx = int(round(center_val - boundary / 2))
            end_ndx = int(start_ndx + boundary)

            # boundaries checking
            if start_ndx < 0:
                log.warning("Crop outside of CT array")

                start_ndx = 0
                end_ndx = boundary

            if end_ndx > ct_image.shape[axis]:
                log.warning("Crop outside of CT array")

                end_ndx = ct_image.shape[axis]
                start_ndx = int(ct_image.shape[axis] - boundary)

            slice_list.append(slice(start_ndx, end_ndx, 1))

        return (ct_image[tuple(slice_list)][:,:, int(center_irc[2])]).astype(np.float32)