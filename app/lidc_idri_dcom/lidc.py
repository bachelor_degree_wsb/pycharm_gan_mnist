import datetime
import glob

from matplotlib import pyplot as plt
from numpy import asarray

from lidc_idri_dcom.lidc_idri_dataset_loader import LidcCtImagesMalignantOnlyDataset
from PIL import Image

def main():
    a = LidcCtImagesMalignantOnlyDataset(3)
    for image, id in a:
        save_slice_window(image, -600, 1500, id)




def save_slice_window(slice, level, window, id):
    max = level + window / 2
    min = level - window / 2
    slice = slice.clip(min, max)

    fig, ax = plt.subplots(1)
    ax.imshow(slice.T, cmap="gray", origin="lower")
    plt.axis('off')
    name=str(datetime.datetime.now().timestamp())
    plt.savefig(fr"E:\lidc-idri\64x64_png\{id}_{name}.png", bbox_inches='tight', pad_inches=0)
    plt.cla()
    plt.clf()

def to_greyscale():
    pngs = glob.glob('../64x64_png/*.png')
    for id, png in enumerate(pngs):
        image = Image.open(png).convert('L')
        image = asarray(image)
        img = Image.fromarray(image)
        img.save(f"./grey/{id}.png")



if __name__ == '__main__':
   to_greyscale()