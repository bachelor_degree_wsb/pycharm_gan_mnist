# 002701_01_03_169.png	2701	1	3	169	128.912, 248.081, 152.782, 250.251, 139.521, 237.954, 137.439, 260.86	123.912, 232.954, 157.782, 265.86	23.9682, 22.9999	0.214427, 0.496326, 0.471386	5	0	84, 217	0.859375, 0.859375, 1	512, 512	-1500, 500	M	55	3
import datetime

from matplotlib import pyplot as plt
from matplotlib.patches import Rectangle
from numpy import asarray
from PIL import Image

def main():
    path = './169.png'
    image = asarray(Image.open(path))
    #123.912, 232.954, 157.782, 265.86
    # image = image[232:265, 123:157]
    save_slice_window(image)

def save_slice_window(slice):
    fig, ax = plt.subplots(1)
    ax.imshow(slice, cmap="gray", origin="lower")
    ax.add_patch(Rectangle((123, 232),
                           33, 24, linewidth=1, edgecolor='r', facecolor='none'))
    plt.show()


if __name__ == '__main__':
    main()