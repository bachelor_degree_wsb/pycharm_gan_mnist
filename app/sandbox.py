from matplotlib import pyplot as plt

from jpg_dataset_loader import LidcCtImagesDataset


def main():
    loader = LidcCtImagesDataset(r'./64x64_png/*.png')
    for data in loader:
        fig, ax = plt.subplots(1)
        ax.imshow(data)
        plt.show()
        print(data.shape)
        break

if __name__ == '__main__':
    main()