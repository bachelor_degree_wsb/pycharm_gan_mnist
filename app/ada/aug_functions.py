import numpy as np
import tensorflow as tf


class AugmentationFunctions:
    def __init__(self, batch_size: int, strength: float):
        self.strength = strength
        self.batch_size = batch_size

    def construct_batch_of_matrices(self, *rows):
        rows = [[tf.convert_to_tensor(x, dtype=tf.float32) for x in r] for r in rows]
        batch_elems = [x for r in rows for x in r if x.shape.rank != 0]
        assert all(x.shape.rank == 1 for x in batch_elems)
        batch_size = tf.shape(batch_elems[0])[0] if len(batch_elems) else 1
        rows = [[tf.broadcast_to(x, [batch_size]) for x in r] for r in rows]
        return tf.transpose(rows, [2, 0, 1])

    def translate_2d(self, tx, ty):
        return self.construct_batch_of_matrices(
            [1, 0, tx],
            [0, 1, ty],
            [0, 0, 1])

    def translate_3d(self, tx, ty, tz):
        return self.construct_batch_of_matrices(
            [1, 0, 0, tx],
            [0, 1, 0, ty],
            [0, 0, 1, tz],
            [0, 0, 0, 1])

    def scale_2d(self, sx, sy):
        return self.construct_batch_of_matrices(
            [sx, 0, 0],
            [0, sy, 0],
            [0, 0, 1])

    def scale_3d(self, sx, sy, sz):
        return self.construct_batch_of_matrices(
            [sx, 0, 0, 0],
            [0, sy, 0, 0],
            [0, 0, sz, 0],
            [0, 0, 0, 1])

    def rotate_2d(self, theta):
        return self.construct_batch_of_matrices(
            [tf.cos(theta), tf.sin(-theta), 0],
            [tf.sin(theta), tf.cos(theta), 0],
            [0, 0, 1])

    def rotate_3d(self, v, theta):
        vx = v[..., 0]
        vy = v[..., 1]
        vz = v[..., 2]
        s = tf.sin(theta)
        c = tf.cos(theta)
        cc = 1 - c
        return self.construct_batch_of_matrices(
            [vx * vx * cc + c, vx * vy * cc - vz * s, vx * vz * cc + vy * s, 0],
            [vy * vx * cc + vz * s, vy * vy * cc + c, vy * vz * cc - vx * s, 0],
            [vz * vx * cc - vy * s, vz * vy * cc + vx * s, vz * vz * cc + c, 0],
            [0, 0, 0, 1])

    def translate_2d_inv(self, tx, ty):
        return self.translate_2d(-tx, -ty)

    def scale_2d_inv(self, sx, sy):
        return self.scale_2d(1 / sx, 1 / sy)

    def rotate_2d_inv(self, theta):
        return self.rotate_2d(-theta)

    def gate_augment_params(self, probability, params, disabled_val):
        shape = tf.shape(params)
        cond = (tf.random.uniform(shape[:1], 0, 1) < probability)
        disabled_val = tf.broadcast_to(tf.convert_to_tensor(disabled_val, dtype=params.dtype), shape)
        return tf.where(cond, params, disabled_val)


class PixelBlitting(AugmentationFunctions):
    def x_flip(self, kernel):
        i = tf.floor(tf.random.uniform([self.batch_size], 0, 2))
        i = self.gate_augment_params(self.strength, i, 0)
        kernel @= self.scale_2d_inv(1 - 2 * i, 1)
        return kernel

    def rotate_90(self, kernel):
        i = tf.floor(tf.random.uniform([self.batch_size], 0, 4))
        i = self.gate_augment_params(self.strength, i, 0)
        kernel @= self.rotate_2d_inv(-np.pi / 2 * i)
        return kernel

    def integer_translation(self, kernel, xint_max: float, width, height):
        t = tf.random.uniform([self.batch_size, 2], -xint_max, xint_max)
        t = self.gate_augment_params(self.strength, t, 0)
        kernel @= self.translate_2d_inv(tf.math.rint(t[:, 0] * width), tf.math.rint(t[:, 1] * height))
        return kernel


class GeometricTransformations(AugmentationFunctions):
    def isotropic_scaling(self, kernel, scale_std):
        s = 2 ** tf.random.normal([self.batch_size], 0, scale_std)
        s = self.gate_augment_params(self.strength, s, 1)
        kernel @= self.scale_2d_inv(s, s)
        return kernel

    def pre_rotation(self, kernel, rotate_max):
        # Before anisotropic scaling.
        p_rot = 1 - np.sqrt(np.cast(np.maximum(1 - self.strength, 0), np.float32))
        theta = tf.random.uniform([self.batch_size], -np.pi * rotate_max, np.pi * rotate_max)
        theta = self.gate_augment_params(p_rot, theta, 0)
        kernel @= self.rotate_2d_inv(-theta)
        return kernel

    def anisotropic_scaling(self, kernel, aniso_std):
        s = 2 ** tf.random.normal([self.batch_size], 0, aniso_std)
        s = self.gate_augment_params(self.strength, s, 1)
        kernel @= self.scale_2d_inv(s, 1 / s)
        return kernel

    def post_rotation(self, kernel, rotate_max):
        # After anisotropic scaling
        p_rot = 1 - np.sqrt(np.cast(np.maximum(1 - self.strength, 0), np.float32))
        theta = tf.random.uniform([self.batch_size], -np.pi * rotate_max, np.pi * rotate_max)
        theta = self.gate_augment_params(p_rot, theta, 0)
        kernel @= self.rotate_2d_inv(-theta)
        return kernel

    def fractional_translation(self, kernel, xfrac_std, width, height):
        t = tf.random.normal([self.batch_size, 2], 0, xfrac_std)
        t = self.gate_augment_params(self.strength, t, 0)
        kernel @= self.translate_2d_inv(t[:, 0] * width, t[:, 1] * height)
        return kernel


class ColorTransformations(AugmentationFunctions):
    def brightness(self, kernel, brightness_std):
        b = tf.random.normal([self.batch_size], 0, brightness_std)
        b = self.gate_augment_params(self.strength, b, 0)
        kernel @= self.translate_3d(b, b, b)
        return kernel

    def contrast(self, kernel, contrast_std):
        c = 2 ** tf.random.normal([self.batch_size], 0, contrast_std)
        c = self.gate_augment_params(self.strength, c, 1)
        kernel @= self.scale_3d(c, c, c)
        return kernel

    def luma_flip(self, kernel):
        I_4 = tf.eye(4, batch_shape=[self.batch_size])
        v = np.array([1, 1, 1, 0]) / np.sqrt(3)  # Luma axis.
        i = tf.floor(tf.random.uniform([self.batch_size], 0, 2))
        i = self.gate_augment_params(self.strength, i, 0)
        i = tf.reshape(i, [self.batch_size, 1, 1])
        kernel @= (I_4 - 2 * np.outer(v, v) * i)  # Householder reflection.
        return kernel


class ImageSpaceTransformations(AugmentationFunctions):
    def space_filtering(self, imgfilter_bands: list, imgfilter_std, num_bands=4):
        assert len(imgfilter_bands) == num_bands
        expected_power = np.array([10, 1, 1, 1]) / 13  # Expected power spectrum (1/f).
        kernel = tf.ones([self.batch_size, num_bands])  # Global gain vector (identity).

        for i, band_strength in enumerate(imgfilter_bands):
            t_i = 2 ** tf.random.normal([self.batch_size], 0, imgfilter_std)
            t_i = self.gate_augment_params(self.strength * band_strength, t_i, 1)
            t = tf.ones([self.batch_size, num_bands])  # Temporary gain vector.
            t = tf.concat([t[:, :i], t_i[:, np.newaxis], t[:, i + 1:]], axis=-1)  # Replace i'th element.
            t /= tf.sqrt(tf.reduce_sum(expected_power * tf.square(t), axis=-1, keepdims=True))  # Normalize power.
            kernel *= t  # Accumulate into global gain.

        return kernel

    def additive_noise(self, noise_std):
        sigma = tf.abs(tf.random.normal([self.batch_size], 0, noise_std))
        sigma = self.gate_augment_params(self.strength, sigma, 0)
        sigma = tf.reshape(sigma, [-1, 1, 1, 1])
        return sigma

    def cutout(self, cutout_size, width, height):
        size = tf.fill([self.batch_size, 2], cutout_size)
        size = self.gate_augment_params(self.strength, size, 0)
        center = tf.random.uniform([self.batch_size, 2], 0, 1)
        size = tf.reshape(size, [self.batch_size, 2, 1, 1, 1])
        center = tf.reshape(center, [self.batch_size, 2, 1, 1, 1])
        coord_x = tf.reshape(tf.range(width, dtype=tf.float32), [1, 1, 1, width])
        coord_y = tf.reshape(tf.range(height, dtype=tf.float32), [1, 1, height, 1])
        mask_x = (tf.abs((coord_x + 0.5) / width - center[:, 0]) >= size[:, 0] / 2)
        mask_y = (tf.abs((coord_y + 0.5) / height - center[:, 1]) >= size[:, 1] / 2)
        mask = tf.cast(tf.logical_or(mask_x, mask_y), tf.float32)
        return mask
