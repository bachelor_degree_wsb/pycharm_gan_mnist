from torch import Tensor
import numpy as np
from ada.aug_manager import AugManager
import tensorflow as tf
import torch

#tune_kimg = 500,
class AdaptiveAugmentation:
    def __init__(self,p: float, tuning_treshold: float, tune_kimg: float, batch_size: int):
        assert p>=0 and p <=1
        assert tuning_treshold>=0 and tuning_treshold <=1

        self.p = p
        self.tune_kimg = tune_kimg
        self.tuning_treshold = tuning_treshold
        self.aug_manager = AugManager(batch_size, p)

    def augment(self, images: Tensor) -> Tensor:
        np_tensor = images.cpu().numpy()
        images = tf.convert_to_tensor(np_tensor)
        images = self.aug_manager.apply_augment(images).numpy()
        return torch.from_numpy(images)

    #nimg_delta = minibatch_size * minibatch_repeats (where size = 32 and repeats = 4)
    #discriminator score is output from Discriminator and input for the loss function
    def tune(self, discriminator_scores: list, nimg_delta):
        rt = self._count_rt(discriminator_scores)
        nimg_ratio = nimg_delta / (self.tune_kimg * 1000)
        p = nimg_ratio * np.sign(rt - self.tuning_treshold)
        self.p = max(p, 0)

    def _count_rt(self, discriminator_scores: list) -> float:
        _rt = 0
        for loss, number in discriminator_scores:
            _rt += np.divide(np.sum(np.sign(loss)), number)

        return np.divide(_rt, len(discriminator_scores))