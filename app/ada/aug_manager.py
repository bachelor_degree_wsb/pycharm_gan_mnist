import numpy as np
import scipy
from numpy import ndarray
import tensorflow as tf

from ada.aug_functions import AugmentationFunctions, GeometricTransformations, PixelBlitting, ColorTransformations, \
    ImageSpaceTransformations


class AugManager(AugmentationFunctions):
    wavelets = {
        'haar': [0.7071067811865476, 0.7071067811865476],
        'db1': [0.7071067811865476, 0.7071067811865476],
        'db2': [-0.12940952255092145, 0.22414386804185735, 0.836516303737469, 0.48296291314469025],
        'db3': [0.035226291882100656, -0.08544127388224149, -0.13501102001039084, 0.4598775021193313,
                0.8068915093133388, 0.3326705529509569],
        'db4': [-0.010597401784997278, 0.032883011666982945, 0.030841381835986965, -0.18703481171888114,
                -0.02798376941698385, 0.6308807679295904, 0.7148465705525415, 0.23037781330885523],
        'db5': [0.003335725285001549, -0.012580751999015526, -0.006241490213011705, 0.07757149384006515,
                -0.03224486958502952, -0.24229488706619015, 0.13842814590110342, 0.7243085284385744, 0.6038292697974729,
                0.160102397974125],
        'db6': [-0.00107730108499558, 0.004777257511010651, 0.0005538422009938016, -0.031582039318031156,
                0.02752286553001629, 0.09750160558707936, -0.12976686756709563, -0.22626469396516913,
                0.3152503517092432, 0.7511339080215775, 0.4946238903983854, 0.11154074335008017],
        'db7': [0.0003537138000010399, -0.0018016407039998328, 0.00042957797300470274, 0.012550998556013784,
                -0.01657454163101562, -0.03802993693503463, 0.0806126091510659, 0.07130921926705004,
                -0.22403618499416572, -0.14390600392910627, 0.4697822874053586, 0.7291320908465551, 0.39653931948230575,
                0.07785205408506236],
        'db8': [-0.00011747678400228192, 0.0006754494059985568, -0.0003917403729959771, -0.00487035299301066,
                0.008746094047015655, 0.013981027917015516, -0.04408825393106472, -0.01736930100202211,
                0.128747426620186, 0.00047248457399797254, -0.2840155429624281, -0.015829105256023893,
                0.5853546836548691, 0.6756307362980128, 0.3128715909144659, 0.05441584224308161],
        'sym2': [-0.12940952255092145, 0.22414386804185735, 0.836516303737469, 0.48296291314469025],
        'sym3': [0.035226291882100656, -0.08544127388224149, -0.13501102001039084, 0.4598775021193313,
                 0.8068915093133388, 0.3326705529509569],
        'sym4': [-0.07576571478927333, -0.02963552764599851, 0.49761866763201545, 0.8037387518059161,
                 0.29785779560527736, -0.09921954357684722, -0.012603967262037833, 0.0322231006040427],
        'sym5': [0.027333068345077982, 0.029519490925774643, -0.039134249302383094, 0.1993975339773936,
                 0.7234076904024206, 0.6339789634582119, 0.01660210576452232, -0.17532808990845047,
                 -0.021101834024758855, 0.019538882735286728],
        'sym6': [0.015404109327027373, 0.0034907120842174702, -0.11799011114819057, -0.048311742585633,
                 0.4910559419267466, 0.787641141030194, 0.3379294217276218, -0.07263752278646252, -0.021060292512300564,
                 0.04472490177066578, 0.0017677118642428036, -0.007800708325034148],
        'sym7': [0.002681814568257878, -0.0010473848886829163, -0.01263630340325193, 0.03051551316596357,
                 0.0678926935013727, -0.049552834937127255, 0.017441255086855827, 0.5361019170917628, 0.767764317003164,
                 0.2886296317515146, -0.14004724044296152, -0.10780823770381774, 0.004010244871533663,
                 0.010268176708511255],
        'sym8': [-0.0033824159510061256, -0.0005421323317911481, 0.03169508781149298, 0.007607487324917605,
                 -0.1432942383508097, -0.061273359067658524, 0.4813596512583722, 0.7771857517005235, 0.3644418948353314,
                 -0.05194583810770904, -0.027219029917056003, 0.049137179673607506, 0.003808752013890615,
                 -0.01495225833704823, -0.0003029205147213668, 0.0018899503327594609],
    }

    def __init__(self, batch_size: int, strength: float):
        super().__init__(batch_size, strength)
        self.I_3 = tf.eye(3, batch_shape=[self.batch_size])
        self.I_4 = tf.eye(4, batch_shape=[self.batch_size])
        self.geo_trans = GeometricTransformations(batch_size, strength)
        self.pixel_blitting = PixelBlitting(batch_size, strength)
        self.color_trans = ColorTransformations(batch_size, strength)
        self.image_space_trans = ImageSpaceTransformations(batch_size, strength)

    def apply_augment(self, images):
        batch, channels, height, width = images.shape.as_list()
        kernel = self.I_3
        kernel = self.pixel_blitting.x_flip(kernel)
        kernel = self.pixel_blitting.rotate_90(kernel)
        kernel = self.pixel_blitting.integer_translation(kernel, 0.125, width, height)
        kernel = self.geo_trans.isotropic_scaling(kernel, 0.2)
        kernel = self.geo_trans.pre_rotation(kernel, 1)
        kernel = self.geo_trans.anisotropic_scaling(kernel, 0.2)
        kernel = self.geo_trans.post_rotation(kernel, 1)
        kernel = self.geo_trans.fractional_translation(kernel, 0.125, width, height)
        images = self.apply_geometric_transformations(kernel, channels, width, height, images)

        kernel = self.I_4
        kernel = self.color_trans.brightness(kernel, 0.2)
        kernel = self.color_trans.contrast(kernel, 0.5)
        kernel = self.color_trans.luma_flip(kernel)
        images = self.apply_color_transformation(kernel, images, channels, height, width)

        kernel = self.image_space_trans.space_filtering([1,1,1,1], 1, 4)
        images = self.apply_image_space_filtering(kernel, images, channels,width, height, 4)

        kernel = self.image_space_trans.additive_noise(0.1)
        images = self.apply_additive_noise(images, channels, height, width, kernel)

        kernel = self.image_space_trans.cutout(0.5, width, height)
        images = self.apply_cutout(images, kernel)

        return images

    def apply_geometric_transformations(self, kernel: ndarray, channels, width, height, images):
        # Execute if the transform is not identity.
        if kernel is not self.I_3:
            # Setup orthogonal lowpass filter.
            Hz = self.wavelets['sym6']
            Hz = np.asarray(Hz, dtype=np.float32)
            Hz = np.reshape(Hz, [-1, 1, 1]).repeat(channels, axis=1)  # [tap, channel, 1]
            Hz_pad = Hz.shape[0] // 4

            # Calculate padding.
            cx = (width - 1) / 2
            cy = (height - 1) / 2
            cp = np.transpose([[-cx, -cy, 1], [cx, -cy, 1], [cx, cy, 1], [-cx, cy, 1]])  # [xyz, idx]
            cp = kernel @ cp[np.newaxis]  # [batch, xyz, idx]
            cp = cp[:, :2, :]  # [batch, xy, idx]
            m_lo = tf.ceil(tf.reduce_max(-cp, axis=[0, 2]) - [cx, cy] + Hz_pad * 2)
            m_hi = tf.ceil(tf.reduce_max(cp, axis=[0, 2]) - [cx, cy] + Hz_pad * 2)
            m_lo = tf.clip_by_value(m_lo, [0, 0], [width - 1, height - 1])
            m_hi = tf.clip_by_value(m_hi, [0, 0], [width - 1, height - 1])

            # Pad image and adjust origin.
            images = tf.transpose(images, [0, 2, 3, 1])  # NCHW => NHWC
            pad = [[0, 0], [m_lo[1], m_hi[1]], [m_lo[0], m_hi[0]], [0, 0]]
            images = tf.pad(tensor=images, paddings=pad, mode='REFLECT')
            T_in = self.translate_2d(cx + m_lo[0], cy + m_lo[1])
            T_out = self.translate_2d_inv(cx + Hz_pad, cy + Hz_pad)
            kernel = T_in @ kernel @ T_out

            # Upsample.
            shape = [self.batch_size, tf.shape(images)[1] * 2, tf.shape(images)[2] * 2, channels]
            images = tf.nn.depthwise_conv2d_backprop_input(input_sizes=shape, filter=Hz[np.newaxis, :],
                                                           out_backprop=images, strides=[1, 2, 2, 1], padding='SAME',
                                                           data_format='NHWC')
            images = tf.nn.depthwise_conv2d_backprop_input(input_sizes=shape, filter=Hz[:, np.newaxis],
                                                           out_backprop=images, strides=[1, 1, 1, 1], padding='SAME',
                                                           data_format='NHWC')
            kernel = self.scale_2d(2, 2) @ kernel @ self.scale_2d_inv(2, 2)  # Account for the increased resolution.

            # Execute transformation.
            transforms = tf.reshape(kernel, [-1, 9])[:, :8]
            shape = [(height + Hz_pad * 2) * 2, (width + Hz_pad * 2) * 2]
            images = tf.contrib.image.transform(images=images, transforms=transforms, output_shape=shape,
                                                interpolation='BILINEAR')

            # Downsample and crop.
            images = tf.nn.depthwise_conv2d(input=images, filter=Hz[np.newaxis, :], strides=[1, 1, 1, 1],
                                            padding='SAME', data_format='NHWC')
            images = tf.nn.depthwise_conv2d(input=images, filter=Hz[:, np.newaxis], strides=[1, 2, 2, 1],
                                            padding='SAME', data_format='NHWC')
            images = images[:, Hz_pad: height + Hz_pad, Hz_pad: width + Hz_pad, :]
            images = tf.transpose(images, [0, 3, 1, 2])  # NHWC => NCHW

            return images

    def apply_color_transformation(self, kernel, images, channels, height, width):
        if kernel is not self.I_4:
            images = tf.reshape(images, [self.batch_size, channels, height * width])
            if channels == 1:
                C = tf.reduce_mean(kernel[:, :3, :], axis=1, keepdims=True)
                images = images * tf.reduce_sum(C[:, :, :3], axis=2, keepdims=True) + C[:, :, 3:]
            else:
                raise ValueError('Image must be RGB (3 channels) or L (1 channel)')
            images = tf.reshape(images, [self.batch_size, channels, height, width])
            return images

    def apply_image_space_filtering(self, kernel, images, channels, width, height, num_bands = 4):
        Hz_lo = self.wavelets['sym2']
        Hz_lo = np.asarray(Hz_lo, dtype=np.float32)  # H(z)
        Hz_hi = Hz_lo * ((-1) ** np.arange(Hz_lo.size))  # H(-z)
        Hz_lo2 = np.convolve(Hz_lo, Hz_lo[::-1]) / 2  # H(z) * H(z^-1) / 2
        Hz_hi2 = np.convolve(Hz_hi, Hz_hi[::-1]) / 2  # H(-z) * H(-z^-1) / 2
        Hz_bands = np.eye(num_bands, 1)  # Bandpass(H(z), b_i)
        for i in range(1, num_bands):
            Hz_bands = np.dstack([Hz_bands, np.zeros_like(Hz_bands)]).reshape(num_bands, -1)[:, :-1]
            Hz_bands = scipy.signal.convolve(Hz_bands, [Hz_lo2])
            Hz_bands[i, (Hz_bands.shape[1] - Hz_hi2.size) // 2: (Hz_bands.shape[1] + Hz_hi2.size) // 2] += Hz_hi2

        # Construct combined amplification filter.
        Hz_prime = kernel @ Hz_bands  # [batch, tap]
        Hz_prime = tf.transpose(Hz_prime)  # [tap, batch]
        Hz_prime = tf.tile(Hz_prime[:, :, np.newaxis], [1, 1, channels])  # [tap, batch, channels]
        Hz_prime = tf.reshape(Hz_prime, [-1, self.batch_size * channels, 1])  # [tap, batch * channels, 1]

        # Apply filter.
        images = tf.reshape(images, [1, -1, height, width])
        pad = Hz_bands.shape[1] // 2
        pad = [[0, 0], [0, 0], [pad, pad], [pad, pad]]
        images = tf.pad(tensor=images, paddings=pad, mode='REFLECT')
        images = tf.nn.depthwise_conv2d(input=images, filter=Hz_prime[np.newaxis, :], strides=[1, 1, 1, 1],
                                        padding='VALID', data_format='NCHW')
        images = tf.nn.depthwise_conv2d(input=images, filter=Hz_prime[:, np.newaxis], strides=[1, 1, 1, 1],
                                        padding='VALID', data_format='NCHW')
        images = tf.reshape(images, [-1, channels, height, width])
        return images

    def apply_additive_noise(self, images, channels, height, width, kernel):
        images += tf.random_normal([self.batch_size, channels, height, width]) * kernel
        return images

    def apply_cutout(self, images, kernel):
        images *= kernel
        return images