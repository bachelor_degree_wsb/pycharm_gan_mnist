import glob

import torch
from numpy import ndarray
from torch.utils.data import Dataset
import logging as log

from ct_image import CtImage


class LidcCtImagesDataset(Dataset):
    def __init__(self, jpgs_path: str,  transform=None):
        self.samples = glob.glob(jpgs_path)
        self.length = len(self.samples)
        self.transform = transform
        self.ct_image = CtImage()

        log.info("{!r}: {} {} samples".format(
            self,
            self.length,
            "training",
        ))

    def __len__(self):
        return self.length

    def __getitem__(self, ndx: int) -> ndarray:
        sample = self.ct_image.retrieve_image(self.samples[ndx])
        if self.transform:
            sample = self.transform(sample)
        return sample

    @classmethod
    def build(cls, transform, path, batch_size, num_workers):
        ds = cls(path, transform)
        return torch.utils.data.DataLoader(ds, batch_size=batch_size,
                                           shuffle=True, num_workers=num_workers)