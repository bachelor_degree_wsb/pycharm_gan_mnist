from numpy import asarray
from numpy import ndarray
from PIL import Image


class CtImage:
    def retrieve_image(self, path: str) -> ndarray:
        return asarray(Image.open(path))