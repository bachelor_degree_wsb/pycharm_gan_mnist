import collections
import csv
import glob
from collections import namedtuple
from typing import List

import torch
from torch import Tensor
from torch.utils.data import Dataset
import logging as log

from luna.ct_image import CtImage
from exception import ImageNotFoundException

CandidateInfoTuple = namedtuple(
    'CandidateInfoTuple',
    'is_nodule, diameter_mm, series_uid, center_xyz, source',
)

XyzTuple = collections.namedtuple('XyzTuple', ['x', 'y', 'z'])

class LunaCtImagesMalignantOnlyDataset(Dataset):
    def __init__(self, images_path: str, annotation_csv_path: str, transform=None):
        self.candidate_info = self._retrieve_malignant_candidate_info_list(annotation_csv_path)
        self.images_path = images_path
        self.transform = transform

        log.info("{!r}: {} {} samples".format(
            self,
            len(self.candidate_info),
            "training",
        ))

    def __len__(self):
        return len(self.candidate_info)

    def __getitem__(self, ndx: int) -> Tensor:
        path = self._retrieve_image_file_path(ndx)
        ct_image = CtImage(path, self.candidate_info[ndx])

        sample = ct_image.retrieve_raw_candidate()
        if self.transform:
            sample = self.transform(sample)

        return sample

    @classmethod
    def build(cls, transform, batch_size, num_workers) -> torch.utils.data.DataLoader:
        annotation_path = '../data/luna/annotations.csv'
        images_path = r"C:\Users\Orion\Desktop\projects\python\pycharm_gan_mnist\data\luna\subset0\\subset0\{}.mhd"
        ds = LunaCtImagesMalignantOnlyDataset(images_path, annotation_path, transform=transform)
        return torch.utils.data.DataLoader(ds, batch_size=batch_size,
                                                      shuffle=True, num_workers=num_workers)
    def _retrieve_image_file_path(self, ndx: int) -> str:
        path = self.images_path.format(self.candidate_info[ndx].series_uid)
        files = glob.glob(path)

        if len(files) == 0:
            raise ImageNotFoundException(f'Image for provided path: "{path}" not found')

        return files[0]

    def _retrieve_malignant_candidate_info_list(self, annotation_csv_path: str) -> List[CandidateInfoTuple]:
        with open(annotation_csv_path, "r") as annotations_file:
            return [
                CandidateInfoTuple(True, float(row[4]), str(row[0]), XyzTuple(*[float(x) for x in row[1:4]]),
                                           'LUNA_DATASET')
                for row in list(csv.reader(annotations_file))[1:]
                if float(row[4]) >= 3.0  # nodule diameter min size
            ]