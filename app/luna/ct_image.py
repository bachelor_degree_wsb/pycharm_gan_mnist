import collections
import logging as log

import SimpleITK as sitk
import numpy as np

from numpy import ndarray

IrcTuple = collections.namedtuple('IrcTuple', ['index', 'row', 'column'])


class CtImage:

    def __init__(self, full_mhd_image_path: str, candidate_info):
        self.candidate_info = candidate_info

        image = sitk.ReadImage(full_mhd_image_path)

        ct_scan = sitk.GetArrayFromImage(image).astype(np.float32)
        # CTs are natively expressed in https://en.wikipedia.org/wiki/Hounsfield_scale
        ct_scan = np.clip(ct_scan, -1000, 1000)
        self.ct_scan = ct_scan
        self.origin = np.array(image.GetOrigin())
        self.spacing = np.array(image.GetSpacing())
        self.direction = np.array(image.GetDirection()).reshape(3, 3)

        self.slice_width_irc = np.array((32, 48, 48))  # range for lung

    def retrieve_raw_candidate(self) -> ndarray:
        center_xyz = self.candidate_info.center_xyz
        center_irc = self.to_irc(center_xyz)

        slice_list = []
        for axis, center_val in enumerate(center_irc):
            # boundaries checking
            assert 0 <= center_val < self.ct_scan.shape[axis], \
                repr([center_xyz, self.origin, self.spacing, center_irc, axis])

            start_ndx = int(round(center_val - self.slice_width_irc[axis] / 2))
            end_ndx = int(start_ndx + self.slice_width_irc[axis])

            # boundaries checking
            if start_ndx < 0:
                log.warning("Crop outside of CT array: {} {}, center:{} shape:{} width:{}".format(
                    self.candidate_info.series_uid, center_xyz, center_irc, self.ct_scan.shape, self.slice_width_irc))
                start_ndx = 0
                end_ndx = int(self.slice_width_irc[axis])

            if end_ndx > self.ct_scan.shape[axis]:
                log.warning("Crop outside of CT array: {} {}, center:{} shape:{} width:{}".format(
                    self.candidate_info.series_uid, center_xyz, center_irc, self.ct_scan.shape, self.slice_width_irc))
                end_ndx = self.ct_scan.shape[axis]
                start_ndx = int(self.ct_scan.shape[axis] - self.slice_width_irc[axis])

            slice_list.append(slice(start_ndx, end_ndx, 1))

        return self.ct_scan[tuple(slice_list)][self.slice_width_irc[0] // 2]

    def to_irc(self, xyz_coord) -> IrcTuple:
        coord_xyz_wo_offset = xyz_coord - self.origin
        scaled_indices_with_voxel_size = coord_xyz_wo_offset @ np.linalg.inv(self.direction)
        coord_cri = scaled_indices_with_voxel_size / self.spacing
        coord_irc = np.round(coord_cri).astype(np.int)[::-1]
        return IrcTuple(*coord_irc)

    # def to_xyz(self, irc_size: IrcTuple) -> ldl.XyzTuple:
    #     coord_cri = irc_size[::-1]  # reverse IRC, to align with XYZ on the output
    #     scaled_indices_with_voxel_size = (coord_cri * self.spacing)
    #     coord_xyz = (self.direction @ scaled_indices_with_voxel_size) + self.spacing
    #     return ldl.XyzTuple(*coord_xyz)
