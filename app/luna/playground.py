from matplotlib import pyplot as plt

from luna.exception import ImageNotFoundException
from luna.luna_dataset_loader import LunaCtImagesMalignantOnlyDataset
from utils import normalize_ct_image


def main():
    annotation_path = '../data/luna/annotations.csv'
    images_path = r"C:\Users\Orion\Desktop\projects\python\pycharm_gan_mnist\data\luna\subset0\\subset0\{}.mhd"
    ds = LunaCtImagesMalignantOnlyDataset(images_path, annotation_path)
    for i in range(0, ds.__len__(), 1):
        try:
            ct_image = ds.__getitem__(i)
        except ImageNotFoundException as e:
            print(e)
            continue


        image = ct_image[int(ct_image.shape[0] // 2)]

        show_slice_window(image, -600, 1500)
        break








# https://theaisummer.com/medical-image-python/#step-2-binarize-image-using-intensity-thresholding !!
def show_slice_window(slice, level, window, normalize=False):
    """
    Function to display an image slice
    Input is a numpy 2D array
    """
    max = level + window / 2
    min = level - window / 2
    slice = slice.clip(min, max)
    if normalize:
        slice = normalize_ct_image(slice)

    # plt.figure()
    # plt.imshow(slice.T, cmap="gray", origin="lower")
    fig, ax = plt.subplots(1)
    # ax.set_aspect('equal')
    ax.imshow(slice)
    # circ = plt.Circle((32, 32), 6.45)
    # ax.add_patch(circ)
    plt.show()
    # plt.savefig('L'+str(level)+'W'+str(window))


if __name__ == '__main__':
    main()
